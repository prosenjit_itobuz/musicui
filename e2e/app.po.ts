import { browser, by, element } from 'protractor';

export class MusicuiPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('musicui-root h1')).getText();
  }
}
