import { MusicuiPage } from './app.po';

describe('musicui App', () => {
  let page: MusicuiPage;

  beforeEach(() => {
    page = new MusicuiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to musicui!!');
  });
});
