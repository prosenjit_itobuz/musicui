import { observable } from 'rxjs/symbol/observable';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/using';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

@Injectable()
export class SliderService {

  constructor() { }

   onDragStart(event, data) {
    event.dataTransfer.setData('data', data);
  }
  //  onDrop(event, data) {
  //   let dataTransfer = event.dataTransfer.getData('data');
  //   event.preventDefault();
  // }
   allowDrop(event) {
    event.preventDefault();
  }



}
