import { Component } from '@angular/core';

@Component({
  selector: 'musicui-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'musicui';
}
