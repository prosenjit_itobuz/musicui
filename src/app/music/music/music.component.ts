import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'musicui-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MusicComponent implements OnInit {
  searchModal = false;

  constructor() { }

  ngOnInit() {
  }

}
