import { Injectable } from '@angular/core';

@Injectable()
export class MusicService {
  albumList = [
    {thumb: 'assets/images/thumb.jpg', id: 1},
    {thumb: 'assets/images/thumb.jpg', id: 2},
    {thumb: 'assets/images/thumb.jpg', id: 3},
    {thumb: 'assets/images/thumb.jpg', id: 4},
    {thumb: 'assets/images/thumb.jpg', id: 5},
    {thumb: 'assets/images/thumb.jpg', id: 6},
    {thumb: 'assets/images/thumb.jpg', id: 7},
    {thumb: 'assets/images/thumb.jpg', id: 8},
    {thumb: 'assets/images/thumb.jpg', id: 9},
    {thumb: 'assets/images/thumb.jpg', id: 10},
  ];

  constructor() { }

  getAlbums() {
    return this.albumList;
  }

}
