import { BehaviorSubject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const KEY = 'AIzaSyBh7Kmt6Ql9Ih4_7MkbwQ-iys8NG2hiBUk';
const API= 'https://www.googleapis.com/youtube/v3'

@Injectable()
export class YoutubeService {
  private trackSub: BehaviorSubject<any> = new BehaviorSubject(null);
  trackObs = this.trackSub.asObservable();

  constructor(
    private http: Http,
    // private jsonp: Jsonp
    ) {
  }

  search(query): Observable<any> {
    console.log(query);
    const queryParams = new URLSearchParams();
    for (let key in query) {
      queryParams.set(key, query[key]);
    }
    const url = `${API}/search?${queryParams}&type=video&part=snippet&maxResults=25&key=${KEY}`;
    
    return this.http.get(url)
      .map(result => {
        return result.json();
      })
      .catch(err => this.handleError(err));
  }

  // autocomplete(query) {
  //   const url = `http://suggestqueries.google.com/complete/search?client=youtube&ds=yt&client=firefox&q=${query}&callback=JSONP_CALLBACK`;

  //   return this.jsonp.request(url)
  //     .map(res => res.json())
  //     .catch(err => this.handleError(err));
  // }

  getFormats(id) {
    const url = `https://yotube-downloader.herokuapp.com/video/${id}`;
    return this.http.get(url)
      .map(res => {
        const result =  res.json();
        console.log(result);
        if (result['formats']) {
          const filterItems = this.getFilterdExt(result['formats'], 'webm');
          const searchType = this.getMatch(filterItems, 'audio');
          delete result.formats;
          result.formats = searchType;
          this.trackSub.next(result);
          console.log(result);
          return true;
        } else {
          return null;
        }
      })
      .catch(err => this.handleError(err));
  }

  private getFilterdExt(formats, ext) {
    return formats.filter(item => item.ext === ext)
  }

  private getMatch(formats, type) {
    return formats.filter(item => new RegExp(type, 'gi').test(item.format))
  }
  private handleError(error: Response | any) {
    console.log(error);
    return Observable.throw(error);
  }

}