import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MusicRoutingModule } from './music-routing.module';
import { MusicComponent } from './music/music.component';
import { MusicService } from './providers/music.service';
import { NavComponent } from './nav/nav.component';

@NgModule({
  imports: [
    CommonModule,
    MusicRoutingModule
  ],
  providers: [
    MusicService
  ],
  declarations: [
    MusicComponent,
    NavComponent
  ]
})
export class MusicModule { }
