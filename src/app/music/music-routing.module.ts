import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MusicComponent } from './music/music.component';

const routes: Routes = [
  {
    path: '',
    component: MusicComponent,
    children: [
      {
        path: 'list', loadChildren: 'app/music/list/list.module#ListModule'
      },
      // {
      //   path: 'details', loadChildren: 'app/music/details/details.module#DetailsModule'
      // },
      { path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      }
    ]
  }]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule
  ]
})
export class MusicRoutingModule { }
