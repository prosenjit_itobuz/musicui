import { Injectable, EventEmitter, ElementRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PlayerService {
  private progressSub: BehaviorSubject<any> = new BehaviorSubject(null);
  progressObs = this.progressSub.asObservable();
  constructor(
  ) { }

}