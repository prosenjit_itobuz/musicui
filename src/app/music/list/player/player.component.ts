import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer,
  ViewChild,
} from '@angular/core';

import { YoutubeService } from '../../providers/youtube.service';
import { PlayerService } from './player.service';

@Component({
  selector: 'musicui-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerComponent implements OnInit, OnDestroy {
  @ViewChild('media') media: ElementRef;
  @ViewChild('disc') disc: ElementRef;
  @ViewChild('progress') progress: ElementRef;
  @Input() image;
  playStatus: boolean = false;
  track: any;

  constructor(
    private playerService: PlayerService,
    private renderer: Renderer,
    private youtubeService: YoutubeService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.youtubeService.trackObs
    .filter(res => res !== null)
    .subscribe(res => {
      this.track = res;
    })
  }

  ngOnDestroy() {
    this.media.nativeElement.remove();
  }

  play() {
    this.media.nativeElement.play();
    this.playStatus = true;
    this.disc.nativeElement.style.animationPlayState = 'running';
  }
  pause() {
    this.media.nativeElement.pause();
    this.playStatus = false;
    this.disc.nativeElement.style.animationPlayState = 'paused';
  }

  // changeProgress() {
  //   const myAudio = this.media.nativeElement;
  //   var duration =  myAudio.duration;
  //   let percent;
  //   myAudio.addEventListener('progress', function() {
  //   if (duration > 0) {
  //     for (var i = 0; i < myAudio.buffered.length; i++) {
  //           if (myAudio.buffered.start(myAudio.buffered.length - 1 - i) < myAudio.currentTime) {
  //             percent = (myAudio.buffered.end(myAudio.buffered.length - 1 - i) / duration) * 100 + "%";
  //             console.log(percent);
  //               break;
  //           }
  //       }
  //   }
  // });

  // myAudio.addEventListener('timeupdate', function() {
  //   var duration =  myAudio.duration;
  //   if (duration > 0) {
  //     percent = ((myAudio.currentTime / duration)*100) + "%";
  //     console.log(percent);
  //   }
  // });
  // }

}
