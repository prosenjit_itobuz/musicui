import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SwiperModule } from 'angular2-useful-swiper';
import { HttpModule, JsonpModule } from '@angular/http';

import { MusicComponentModule } from '../components/components.module';
import { SliderService } from '../../providers/slider.service';
import { ListComponent } from './list/list.component';
import { PlayerComponent } from './player/player.component';
import { PlayerService } from './player/player.service';
import { YoutubeService } from '../providers/youtube.service';

const routes: Routes = [
  {
    path: '',
    component: ListComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SwiperModule,
    HttpModule,
    JsonpModule,
    MusicComponentModule
  ],
  declarations: [
    ListComponent,
    PlayerComponent
  ],
  providers: [
    SliderService,
    PlayerService,
    YoutubeService
  ],
  exports: [
    RouterModule
  ]
})
export class ListModule { }
