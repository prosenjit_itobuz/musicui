import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { SliderService } from '../../../providers/slider.service';
import { MusicService } from '../../providers/music.service';
import { YoutubeService } from '../../providers/youtube.service';


@Component({
  selector: 'musicui-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnInit {
  albumsList;
  modalSong;
  showDialog = false;
  searchModal = false;
  config: SwiperOptions = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
    slidesPerView: 3
  };
  modalThumb;
  fetching: boolean;

  constructor(
    private musicService: MusicService,
    private sliderService: SliderService,
    private youtubeService: YoutubeService,
    private changeDetector: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.search(params);
    })
  }

  search(query) {
    const queryEncode = encodeURIComponent(JSON.stringify(query));
    this.youtubeService.search(query).subscribe(res => {

      this.albumsList = res.items.map(item => {
        const image = this.sanitizer.bypassSecurityTrustStyle(`url(${item.snippet.thumbnails.high.url}`);
        item.image = image;
        return item;
      });
      this.changeDetector.detectChanges();
    })
  }

  openModal(image, id) {
    if (this.fetching) {
      return;
    } else {
      this.fetching = true;
    }
    this.youtubeService.getFormats(id).subscribe(res => {
      this.modalThumb = image;
      this.showDialog = !this.showDialog
      this.fetching = false;
      this.changeDetector.detectChanges();
    })
  }


}
